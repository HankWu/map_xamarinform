﻿using System;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;
using Xam.Plugin.Abstractions;

using System.Reflection;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using RestSharp;
using System.Diagnostics;

namespace XFDevGPS1
{
	public partial class XFDevGPS1Page : ContentPage
	{
        ETrackDatabase eTrackDatabase;
		string cloudBaseUrl = "http://52.26.88.189";
		string cloudPath = "/bigdata/eld/upload";

		public XFDevGPS1Page()
		{
			InitializeComponent();

            eTrackDatabase = new ETrackDatabase();
			eTrackDatabase.DeleteAll();


			Task.Run(() => StartListening());

            var htmlSource = new HtmlWebViewSource();
			htmlSource.BaseUrl = DependencyService.Get<IBaseUrl>().Get();
			htmlSource.Html = loadMapHtml();

            WebMap.Source = htmlSource;
            WebMap.VerticalOptions = LayoutOptions.FillAndExpand;
		}


        private String loadMapHtml() {
			var assembly = typeof(XFDevGPS1Page).GetTypeInfo().Assembly;
			Stream stream = assembly.GetManifestResourceStream("XFDevGPS1.map.html");

			string text = "";
			using (var reader = new System.IO.StreamReader(stream))
			{
				text = reader.ReadToEnd();
			}
            return text;
        }

		async Task StartListening()
		{
			await CrossGeolocator.Current.StartListeningAsync(1, 0.001);

			CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
		}

		async Task UploadToCloud(ETrackItem[] items)
		{
			// generate a json array
			string totalString = "[";
			for (int i = 0; i < items.Length; i++ )
			{
				if(i!=0)
					totalString += ",";
				
				totalString += new EtrackJson(items[i]).jsonString;
			}
			totalString += "]";

			// upload
			var client = new RestClient(cloudBaseUrl);
			var request = new RestRequest(cloudPath, Method.POST);
			string filename = "log.txt";
			request.AddFile("file", Encoding.UTF8.GetBytes(totalString), filename);

			var respone = client.ExecuteAsync<VehicleCloudResponse>(request, (IRestResponse<VehicleCloudResponse> arg1, RestRequestAsyncHandle arg2) =>
			{
				VehicleCloudResponse data = arg1.Data;
				var result = data.message;

				Device.BeginInvokeOnMainThread(() =>
				{
					uploadResultLabel.Text = result;
				});
			});
		}



		// WebView javascript function
        private void SetMark(double lat, double lng) {
			WebMap.Eval(string.Format("setMark({0}, {1})", lat, lng));
		}

        private void SetCenter(double lat, double lng) {
			WebMap.Eval(string.Format("newLocation({0}, {1})", lat, lng));
        }

        private void MoveMark(double lat, double lng) {
            WebMap.Eval(string.Format("moveMark({0}, {1})", lat, lng));
        }
		//

		private GPSStatus gpsStatus;
		private int numberOfETrackItems = 0;

		private void Current_PositionChanged(object sender, Plugin.Geolocator.Abstractions.PositionEventArgs e)
		{
			var position = e.Position;

			if (null == gpsStatus)
			{
				gpsStatus = new GPSStatus
				{
					lat = position.Latitude,
					lng = position.Longitude,
					speed = Convert.ToInt32(position.Speed),
					timestamp = position.Timestamp.ToLocalTime().ToString(),
				};
			}
			else
			{
				gpsStatus.update(position.Latitude, position.Longitude, Convert.ToInt32(position.Speed), position.Timestamp.ToLocalTime().ToString());
			}

			eTrackDatabase.SaveItem(new ETrackItem
			{
				car_id = "",
				user_id = 26,
				gps_latitude = gpsStatus.lat,
				gps_longitude = gpsStatus.lng,
				mileage = gpsStatus.mileage,
				vehicle_speed = gpsStatus.speed,
				dt = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds,
            });
			numberOfETrackItems++;

			Device.BeginInvokeOnMainThread(() =>
			{
				databaseInfoLabel.Text = $"{numberOfETrackItems}";

				gpsInfoLabel.Text = "Full: Lat: " + gpsStatus.lat.ToString() + " Long: " + gpsStatus.lng.ToString();
				gpsInfoLabel.Text += "\n" + $"Time: {gpsStatus.timestamp}";
				gpsInfoLabel.Text += "\n" + $"Speed: {gpsStatus.speed.ToString()}";
				gpsInfoLabel.Text += "\n" + $"Mileage: {gpsStatus.mileage}";

			});

			if (numberOfETrackItems >= 20)
			{
				ETrackItem[] eTrackItems = eTrackDatabase.GetItems().ToArray();
				eTrackDatabase.DeleteAll();
				numberOfETrackItems = 0;
				Task.Run(() => UploadToCloud(eTrackItems));
			}
            MoveMark(position.Latitude, position.Longitude);
		}

	}
}
